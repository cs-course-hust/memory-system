# 层次结构

一些计算机先驱准确地预测到程序员肯定会希望拥有无限数量的快速存储器。满足这一愿望的一种经济型解决方案是**存储器层次结构**，这种方式利用了局域性原理，并在存储器技术的性能与成本之间进行了折中。前面介绍的**局部性原理**是指，大多数程序都不会均衡地访问所有代码或数据。局域性可以在时间域发生(即**时间局部性**)，也可以在空间域发生(即**空间局部性**)。根据这一原理和"在给定实现技术和功率预算的情况下，硬件越小，速度可以越快"的准则，就产生了存储器层次结构，这些层次由不同速度、不同大小的存储器组成。图2-1显示了一个多级存储器层次结构，包括典型大小和访问速度。

![memory-hierachy](figures/memory-hierarchy.svg)

由于快速存储器非常昂贵，所以将存储器层次结构分为几个级别一越接近处理器，容量越小、速度越快、每字节的成本也越高。其目标是提供一种存储器系统，每字节的成本儿乎与最便宜的存储咤级别相同，速度几乎与最快速的级别相同。在大多数(但并非全部)情况下，低层级存储器中的数据是其上一级存储器中数据的超集。这一性质称为包含性质，层次结构的最低级别必须具备这一性质，如果这一最低级别是缓存，则由主存借器组成，如果是虚拟存傅器，则由磁盘存储器组成。随着处理器性能的提高，存储器层次结构的重要性也在不断增加。图2-2是单处理器性能和主存储器访问次数的历史发展过程。处理器曲线显示了平均每秒发出存储器请求数的增加(即两次存储器引用之间延迟值的倒数)，而存储器曲线显示每秒DRAM访问数的增加(即DRAM访问延迟的倒数)。在单处理器中，由于峥值存储器访问速度要快于平均速度(也就是图中绘制的速度)，所以其实际情况要稍差一些。
