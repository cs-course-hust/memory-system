## 基本缓存优化方法小结

本节介绍了用于降低缺失率与缺失代价、缩短命中时间的技术，这些技术通常会影响到存储器平均访问公式的其它部分，还会影响到存储器层次结构的复杂性。表B-8总结了这些技术，并估计了对复杂性的影响，"+"表示该技术对该因素有改进，"-"表示该技术对该因素有伤害，空白表示没有影响。本图中任何一项优化方法都不能对一个以上的类别提供帮助。

表B-9 关于本章介绍的技术，基本缓存优化对缓存性能和复杂度的影响汇总

| 技术                           | 命中时间 | 缺失代价 | 缺失率 | 硬件复杂度 | 备注                                           |
|:-----------------------------|:--------:|:--------:|:------:|:----------:|:---------------------------------------------|
| 增大块大小                     |          |    -     |   +    |     0      | 微小，Pentium使用128字节                        |
| 增大缓存                       |    -     |          |   +    |     1      | 广泛应用，特别是对L2缓存                        |
| 提高相联度                     |    -     |          |   +    |     1      | 广泛应用                                       |
| 采用多级缓存                   |          |    +     |        |     2      | 昂贵的硬件，若$L1块大小 \neq L2块大小$，广泛应用 |
| 读操作优先于写操作             |          |    +     |        |     1      | 广泛应用                                       |
| 避免在缓存所引期间进行地址转换 |    +     |          |        |     1      | 广泛应用                                       |

\* 从整体上来说，一项技术只能改善一个因素。"+"意味着该技术对该因素有改进，"-"意味着其对该因素有伤害，空白表示没有影响。复杂度是主管的，"0"表示最容易，"3"表示最富挑战性。
