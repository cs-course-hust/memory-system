# 混合内存系统

在大数据背景下，计算机系统对内存容量的需求越来越大。传统基于动态随机存取存储器（Dynamic Random Access Memory，DRAM）的内存系统在制造工艺和能耗上正面临着巨大的挑战，难以满足大数据时代的需求。随着材料和存储技术的迅速发展，以相变存储器（Phase Change Memory，PCM）和阻变存储器（Resistive Random Access Memory，RRAM）为代表的新型非易失内存（Non-Volatile Memory，NVM）开始出现。NVM 具有掉电非易失、按字节修改寻址、快速访存、高存储密度、低静态功耗等优点，这些优良特性让 NVM 可以和 DRAM 一起组成混合内存系统，甚至在未来完全取代 DRAM 成为内存系统的主要存储介质。
