# 非层次结构存储系统

存储分层结构的根基，在于应用一侧的局部性特征，以及系统一侧的成本性能折衷，不过，伴随着新型存储原理和技术的出现，提供了兼顾主存储器性能和次级存储器非易失性及容量的新型存储设备，在计算机系统结构上首次出现了模糊此处存储分级结构的机遇，即实现一种不需要对主存和外存进行严格区分的存储系统，为此需要探讨其访存方法的改变及相应系统设计

新型非易失存储器并非全部都是优点，也不可能完全取代成熟的动态存储器乃至静态存储器，而是各取所长形成一种混合结构，其中最关键的问题在于其读写不对称性与磨损不均衡性，为此也需要进一步进行探讨，应如何考虑这些因素，设计相应的混合内存系统
